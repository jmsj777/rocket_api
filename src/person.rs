use rocket_contrib::json::{Json, JsonValue};
use serde::{Deserialize, Serialize};

#[path = "db/conexao.rs"]
mod conexao;

#[path = "db/dao.rs"]
mod dao;

#[derive(Serialize, Deserialize, Debug)]
pub struct Person {
  name: String,
  age: i32,
  date: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct P {
  id: i32,
}

#[post("/person", format = "json", data = "<p>")]
pub fn get(p: Json<P>) -> JsonValue {
  match dao::get_one(conexao::GET_PERSON, p.id) {
    Ok(r) => {
      let mut res = Vec::new();
      for row in r {
        res.push(Person {
          name: row.get(1),
          age: row.get(2),
          date: row.get(3),
        });
      }
      json!(res)
    }
    Err(e) => e,
  }
}

#[get("/person/getall")]
pub fn get_all() -> JsonValue {
  match dao::query(conexao::GET_PEOPLE) {
    Ok(r) => {
      let mut res = Vec::new();
      for row in r {
        res.push(Person {
          name: row.get(1),
          age: row.get(2),
          date: row.get(3),
        });
      }
      json!(res)
    }
    Err(e) => e,
  }
}

#[post("/person/create", format = "json", data = "<p>")]
pub fn create(p: Json<Person>) -> JsonValue {
  match conexao::get_conn() {
    Ok(mut conn) => match conn.execute(conexao::INSERT_PERSON, &[&p.name, &p.age, &p.date]) {
      Ok(r) => json!(r),
      Err(e) => json!({ "Error": e.to_string() }),
    },
    Err(e) => {
      json!({"Connection Error": e.to_string() })
    }
  }
}
