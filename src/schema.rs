use rocket_contrib::json::JsonValue;

#[path = "db/conexao.rs"]
mod conexao;
#[path = "db/dao.rs"]
mod dao;

#[get("/create_tables")]
pub fn create_tables() -> JsonValue {
  dao::execute(conexao::SCHEMA)
}

#[allow(dead_code)]
#[get("/drop_tables")]
pub fn drop_tables() -> JsonValue {
  dao::execute(conexao::DROP_ALL)
}
