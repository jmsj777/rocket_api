#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate rocket_contrib;

mod person;
mod schema;

fn main() {
    rocket::ignite()
        .mount(
            "/api",
            routes![
                person::get,
                person::create,
                person::get_all,
                schema::create_tables,
                //schema::drop_tables,
            ],
        )
        .launch();
}
